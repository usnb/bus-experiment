#!/usr/bin/env node

//http://www.collectionsjs.com
var Dict = require("collections/dict");
var Sey = require("collections/set");
var sender = require('./amqp-sender');
var Message = require('scb-node-parser/message');
var validator = require('validator');

var winston = require('winston');

winston.loggers.add('facebookmessenger', {
    file: {
        filename: 'facebook_messenger.log'
    }
});

winston.loggers.add('email', {
    file: {
        filename: 'email.log'
    }
});

winston.loggers.add('twittertimeline', {
    file: {
        filename: 'twitter_timeline.log'
    }
});

winston.loggers.add('twitterdirectmessage', {
    file: {
        filename: 'twitter_directmessage.log'
    }
});

/** 
 * Database
 * TODO: replace it for a real one!
 **/


 //socialbus userId and their social media identities
 //one user can have multiple identitites
var set = new Set();
set.add({ userId: 'carmen', identity: 'carmenatinria@gmail.com'});
set.add({ userId: 'clara', identity: 'claraatinria'});
set.add({ userId: 'silvia', identity: 'silviaatinria'});
set.add({ userId: 'john', identity: 'johnatinria@gmail.com'});
set.add({ userId: 'rafael', identity: '1970087563025359'});
set.add({ userId: 'rafael', identity: 'rafaelangarita.at.bus@gmail.com'});
set.add({ userId: 'jorge', identity: '1464938140276735'});
set.add({ userId: 'EliZu', identity: '844967722293814'});



//social media identities and their corresponding platform.
//In this version, each platform corresponds to a RabbitMQ exchange and to a persona
var socialmedia = new Dict();
socialmedia.set('rafaelangarita.at.bus@gmail.com', 'email');
socialmedia.set('carmenatinria@gmail.com', 'email');
socialmedia.set('claraatinria', 'twitter');
socialmedia.set('silviaatinria', 'twittertimeline');
socialmedia.set('johnatinria@gmail.com', 'email');
socialmedia.set('1970087563025359', 'messenger'); //rafael
socialmedia.set('1464938140276735', 'messenger'); //jorge
socialmedia.set('844967722293814', 'messenger'); //EliZu

//Preferences: one identity per socialbus user
var preferences = new Dict();
preferences.set('carmen', 'carmenatinria@gmail.com');
preferences.set('clara', 'claraatinria');
preferences.set('silvia', 'silviaatinria');
preferences.set('john', 'johnatinria@gmail.com');
preferences.set('rafael', '1970087563025359');
preferences.set('jorge', '1464938140276735');
preferences.set('EliZu', '844967722293814');

//Profiles: socialbus user profiles
var profiles = new Dict();
profiles.set('carmen', 
                {userId: 'carmen',
                about: 'Civic Engagement adviser',
                picture: 'https://preview.ibb.co/mBxSaT/handprint_472090_1280.jpg'
                });
profiles.set('john', {userId: 'john',
                about: 'I like programming.',
                picture: 'https://preview.ibb.co/kF6W28/shoe_print_3482282_1280.jpg'});
profiles.set('rafael', {userId: 'rafael',
                about: 'Nothing yet.',
                picture: 'https://preview.ibb.co/kF6W28/shoe_print_3482282_1280.jpg'});
profiles.set('EliZu', {userId: 'EliZu',
                about: 'Nothing yet.',
                picture: 'https://preview.ibb.co/mBxSaT/handprint_472090_1280.jpg'});
profiles.set('jorge', {userId: 'jorge',
                about: 'Nothing yet.',
                picture: 'https://preview.ibb.co/kF6W28/shoe_print_3482282_1280.jpg'});

/** 
 * END of Database
 **/

 /**
  * It returns the socialbus userId given an identity
  **/
function getUserId(identity, callback) {

    //socialbus persona acting as a proxy for closed platforms such as Messenger
    //may deal directly with socialbus userIds so 'identity' is already the userId
    if(preferences.get(identity) != undefined) { 
        callback(identity);
    } else {
        set.forEach(function(value, key) {
            console.log(identity + ' === ' + key.identity + " for key " + JSON.stringify(key));
            if(identity === key.identity) {
                callback(key.userId);
            }
        });
    }
}

function process(message) {

    var log = winston.loggers.get(message._persona);
    log.log('info', 'received_message', message);

    switch (message.getType()) {
        case 'ERROR':
            //findNetwork(message, network => {
                returnError(message, message._persona);
            //});

            break;
        case 'COMMAND':
            //findNetwork(message, network => {
                executeCommand(message, message._persona);
            //});
            break;
        case 'TO':
            message.getTo().forEach(k => {
                var persona;
                var response;
                console.log()
                /** 
                 * john is a bot. He answers the same thing over and over again
                 **/
                if(k.trim() === 'john') {
                    console.log('we have a winner');
                    //winning message
                    winningResponse = new Message(message.getFrom(), {
                        name: 'john',
                        uniqueName: 'john'
                    },  '','Hi, I\'m John! We can finally communicate!', 'TO', message._persona);
                    winningPersona = message._persona;
                    sender.post(winningResponse, winningPersona);

                } else {

                    //get the information of the receiver
                    getUserId(k, (userId) => {
                        persona = socialmedia.get(preferences.get(userId));
                        console.log('to ' + userId + ' who uses ' + persona);
                        //get the information of the sender
                        getUserId(message.getFrom().uniqueName, (userIdSender) => {
                                msg = new Message({
                                    name: userId,
                                    uniqueName:  preferences.get(userId)
                                }, {
                                    name: userIdSender,
                                    uniqueName:  preferences.get(userIdSender)
                                }
                                ,  'Conversation with ' + userIdSender, message.getMessage(), 'TO', message._persona);
                                msg._persona = message._persona;
                                sender.post(msg, persona);
                                var log = winston.loggers.get(message._persona);
                                log.log('info', 'sent_message', msg);
                        });
                    });
                        
                        
                  }

              //TODO: redo this case. place it somewhere
               /* } else {
                    var err = 'I don\'t know user ' + k + ':(';
                    response = new Message(message.getFrom(), {
                        name: 'socialbus',
                            uniqueName: 'socialbus'
                    }, 'Error', err, 'INFO', message._persona);
                    persona = message._persona;
                }*/
                
            })
            break;
        default:

    }
}
module.exports.process = process;

//The rest of the functions are probably deprecated and they are going to dissapear
function executeCommand(message, network) {
    console.log('executing command %s for %s', message.getMessage(), JSON.stringify(message.getFrom()));
    var receivedMesasage = message;
    var message;
    switch (message.getMessage().toLowerCase()) {
        case 'about':
            console.log('executing command about');
            var response = {};
            response.text = 'Hi! I\'m Socialbus.';
            response.text  += '\n I\'m the version 1.0';
            response.text  += '\n See you soon!';
            response.picture = 'https://preview.ibb.co/jtM978/twitter_292988_1920.jpg';
            response.url = 'https://gitlab.inria.fr/usnb/universal-social-network-bus';
            message = new Message(message.getFrom(), {
                name: 'socialbus',
                uniqueName: 'socialbus'
                }, 'About me', response,
                'ABOUT', message._persona);
            break;
        case 'help':
            console.log('executing command help');
            var response = 'Hi! I\'m Socialbus, here you have some basic usage information.';
            response += '\n #help: returns basic usage information.';
            response += '\n #about: returns additional general information about the online social network bus.';
            response += '\n #ls: returns the list of users registered in the online social network bus.';
            response += '\n #more userName: returns additional information about the user \'userName\'.';
            response += '\n #to={userName1,..,userNameN}: sends a message to users specified in the list.';
            message = new Message(message.getFrom(), {
                    name: 'socialbus',
                    uniqueName: 'socialbus'
                }, 'Some help', response,
                'INFO', message._persona);
            break;
        case 'ls': //List all users
            console.log('executing command ls');
            var response = [];
            //if the user requesting the list of socialbus users is not
            //registered, response will be empty since getUserId
            //won't return anything
            getUserId(receivedMesasage.getFrom().uniqueName , (userId) => {
                profiles.forEach(k => {        
                    if(k.userId != userId)
                        response.push(k);
                });
            })
            message = new Message(message.getFrom(), {
                    name: 'socialbus',
                    uniqueName: 'socialbus'
                }, 'User list', response,
                'LS', message._persona);
            break;
        case 'idea':
            console.log('executing command idea');
            var response = 'Thank you for submitting an idea!';
            message = new Message(message.getFrom(), {
                    name: 'socialbus',
                    uniqueName: 'socialbus'
                }, 'User list', response,
                'INFO', message._persona);
            break;
        case 'vote':
            console.log('executing command vote from ' + JSON.stringify(message.getFrom().uniqueName));
            var response = 'Thank you for voting!';
            message = new Message(message.getFrom(), {
                    name: 'socialbus',
                    uniqueName: 'socialbus'
                }, 'User list', response,
                'INFO', message._persona);
            break;
        
        default:
            var commandList = message.getMessage().split(' ');
            if (commandList[0] === 'more') {
                var response = '';
                if (commandList[1] === 'john') {
                    response = commandList[1] + ' is a guitar player at Inria.';
                } else if (commandList[1] === 'carmen') {
                    response = commandList[1] + ' the president of Moonland.';

                } else {
                    response = 'I\'m sorry, I don\'t know any ' + commandList[1];
                }
                message = new Message(
                    message.getFrom(), {
                        name: 'socialbus',
                        uniqueName: 'socialbus'
                    }, 'More about ' + commandList[1], response,
                    'INFO', message._persona);
            }
    }
    var log = winston.loggers.get(message._persona);
    log.log('info', 'sent_message', message);
    sender.post(message, network);
}

function returnError(message, network) {
    var response = 'I didn\'t understand. Try with "#help".';
    response += '\n See you soon! \n Socialbus';
    var message = new Message(message.getFrom(), {
            name: 'socialbus',
            uniqueName: 'socialbus'
        }, 'Ooops!', response,
        'INFO');
    var log = winston.loggers.get(message._persona);
    log.log('info', 'sent_message', message);
    sender.post(message, network);
}

function getUserFromMedia(source) {
    var result = '';
    //try to find user by current app id
    dict.keys().forEach(k => {
        if (dict.get(k).toString().trim() === source.toString().trim()) {
            result = k;
        }
    });

    if (result === '') {
        result = 'Unknow user: ' + source;
    }
    return result;
}
