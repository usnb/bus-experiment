const main = require('../main.js');
const chai = require('chai');
var rewire = require('rewire');
const assert = chai.assert;

//use rewire to invoke the non-exported function 'parse'
const app = rewire('../main.js');
getUserFromMedia = app.__get__('getUserFromMedia');

describe('Managing registered users: ', function() {
    it('gets a user scb id', function() {
        var data = 'angarita.rafael@gmail.com';
        var result = getUserFromMedia(data);

        assert.equal(result, 'rafael');

        //unknown user
        data = 'x@gmail.com';
        result = getUserFromMedia(data);

        assert.equal(result, 'Unknow user: ' + 'x@gmail.com');
    });;

})
