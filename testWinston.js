var winston = require('winston');

winston.loggers.add('category1', {
    file: {
        filename: 'category1'
    }
});

winston.loggers.add('category2', {
    file: {
        filename: 'category2'
    }
});

var category1 = winston.loggers.get('category1');

category1.info('111 logging from your IoC container-based logger');

var category2 = winston.loggers.get('category2');

category2.info('XXX logging from your IoC container-based logger');
